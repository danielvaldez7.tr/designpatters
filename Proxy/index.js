const bcrypt = require("bcrypt");
const saltRound = 10;

const jwt = require("jsonwebtoken");
const fs = require("fs");

/**
 * Simula un servidor de fondo.
 */
class ServidorFondo {
	constructor(request) {}

	response(recurso) {
		if (recurso === "products") {
			return {
				code: 200,
				error: false,
				products: [
					{ name: "leche", precio: 34 },
					{ name: "pan", precio: 60 },
					{ name: "fresas", precio: 53 },
					{ name: "sabritones", precio: 15 },
				],
			};
		}

		if (recurso === "categories") {
			return {
				code: 200,
				error: false,
				categories: [
					{ name: "Lacteos", descripcion: "Leche, quesos" },
					{ name: "Carbohidratos", descripcion: "Pan" },
					{ name: "Frutas", descripcion: "frutas frescas de temporada" },
					{ name: "Botanas", descripcion: "productos fritos" },
				],
			};
		}
	}
}

/**
 * Clase para simular un proxy reverso. Sera el intermediario de las peticiones a la intranet.
 */
class ReverseProxy {
	constructor(request, handler) {
		this.handler = handler;
		this.request = request;
	}

	/**
	 * Esta funcion verifica la peticion del cliente. Autentifica al usuario y valida las credenciales.
	 * @return object: response.
	 */
	async authorizeAccess() {
		const { headers, data } = this.request;
		try {
			let decodedToken = await this.handler.verifyToken(headers.token);
			// if (!decodedToken) throw new Error("El token no es valido");

				let isEquals = await this.handler.comparePassword(
					data.password,
					decodedToken.password
				);

				if (isEquals) {
					const { target } = this.request;

					let splitString = target.split("/");
					let resource = splitString[splitString.length - 1];

					switch (resource) {
						case "products":
							return new ServidorFondo().response("products");
							break;
						case "categories":
							return new ServidorFondo().response("categories");
							break;
					}
				
				// 	return {
				// 		code: 403,
				// 		error: false,
				// 		body: "Credenciales incorrectas!",
				// 	};
			   }
				return {
					code: 403,
					error: false,
					body: "Credenciales incorrectas!",
				};
			
		} catch (e) {
			console.log(e.name);
			return {
				code: 403,
				error: false,
				body: "Acceso denegado: " + e.name,
			};
		}
	}
}

//Objeto manejador: Sirve para hacer validaciones.
const handler = {
	/**
	 * Verifica y decodifica el token pasado como parametro.
	 * @return Boolean
	 */
	verifyToken: function (token) {
		if (token) {
			return jwt.verify(token, PUBKEY, { algoritmh: "RS256" });
		} else {
			console.log("No existen token .");
		}
	},

	/**
	 * Compara si las contraseñas coinciden.
	 * @return Boolean
	 */
	comparePassword: async function (password, encryptPassword) {
		return await bcrypt.compare(password, encryptPassword);
	},
};

//Importamos el token:
/*
 *El token tiene una duracion de 365 dias. Esta firmado con un algoritmo RS256.
 *
 * */
const TOKEN = require("./util");

//importamos la llave publica.
const PUBKEY = fs.readFileSync(__dirname + "/keys/id_rsa_pub.pem");

//Esta funcion anonima simula un cliente http.
(async () => {
	try {
		//Creamos un objeto: request y lo inicializamos.
		let request = {
			source: "http://195.156.44.2:80/",
			target: "http://10.56.3.4:443/products",
			method: "POST",
			headers: { token: TOKEN },
			data: {
				email: "daniel@security.site",
				password: "qwerty",
			},
		};

		//Servidor de fondo (emulacion).
		let auth = new ServidorFondo(request);

		console.log(
			"******************************************************************"
		);
		console.log("Peticion a la ruta: http://10.56.3.4:443/products");
		console.log(
			"******************************************************************"
		);

		let reverseProxy = new ReverseProxy(request, handler);
		let response = await reverseProxy.authorizeAccess();
		console.log(response);
		console.log(
			"******************************************************************\n"
		);

		console.log(
			"\n******************************************************************"
		);
		console.log("Peticion a la ruta: http://10.56.3.4:443/categories");
		console.log(
			"******************************************************************"
		);

		//Modificamos el objeto request:
		request.target = "http://10.56.3.4:443/categories";
		// console.log(reverseProxy);
		let response2 = await reverseProxy.authorizeAccess();
		console.log(response2);
		console.log(
			"******************************************************************\n"
		);

		console.log(
			"\n******************************************************************"
		);
		console.log("Peticion a la ruta: http://10.56.3.4:443/categories");
		console.log("\tConsumiendo la API: Ingresando una contraseña incorrecta!");
		console.log(
			"******************************************************************"
		);

		//Modificamos el objeto request: ingresando una contraseña incorrecta.
		request.data.password = "mypassword";
		// console.log(reverseProxy);
		let response3 = await reverseProxy.authorizeAccess();
		console.log(response3);

		console.log(
			"******************************************************************\n"
		);

		console.log(
			"\n******************************************************************"
		);
		console.log("Peticion a la ruta: http://10.56.3.4:443/categories");
		console.log("\tConsumiendo la API: Ingresando un token incorrecto!");
		console.log(
			"******************************************************************"
		);

		//Modificamos el objeto request: ingresando un token incorrecto.
		request.headers.token = "fjfasjdfFASDFakjsdfasdfkj. djaskdlfjkajsdf.3r99efduoiasjdifoj";
		
		let response4 = await reverseProxy.authorizeAccess();
		console.log(response3);

		console.log(
			"******************************************************************\n"
		);
	} catch (e) {
		console.log(e);
	}
})();
