// 1.- Opcion 1: Que infiera el resultado 

//Plantilla para manejar las respuestas http. Este modelo es a nivel repository. y lo tiene que formatear a un objeto de dominio o entity
//Preguntate en que capa se maneja el adapter?

/*Objeto concretos:*/

//Objeto a nivel repositorio
type employeeSql = {
    id: number;
    name?: string;
    email?: string;
}

//Objeto a nivel entity (Entidad de negocio)
class Employee {
    private id: number | undefined;
    private name: string | undefined;
    private email: string | undefined;

    constructor(id?: number, name?: string, email?: string) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    setId(id: number) {
        this.id = id
    }

    getId() {
        return this.id;
    }

    setName(name: string) {
        this.name = name;
    }

    getName() {
        return this.name;
    }

    setEmail(email: string) {
        this.email = email;
    }

    getEmail() {
        return this.email;
    }
}


interface IReposity<modelSql> {
    getAll(): modelSql[];
    createOne(entity: modelSql): modelSql;
    getOneById(entity: modelSql): modelSql;
    replaceOne(entity: modelSql): modelSql;
    deleteOne(entity: modelSql): modelSql;
}

class MysqlRepository<modelSql> implements IReposity<modelSql>{
    getAll(): modelSql[] {
        throw new Error("Method not implemented.");
    }
    createOne(entity: modelSql): modelSql {
        throw new Error("Method not implemented.");
    }
    getOneById(entity: modelSql): modelSql {
        throw new Error("Method not implemented.");
    }
    replaceOne(entity: modelSql): modelSql {
        throw new Error("Method not implemented.");
    }
    deleteOne(entity: modelSql): modelSql {
        throw new Error("Method not implemented.");
    }

}

class PostgresRepository<modelSql> implements IReposity<modelSql>{
    getAll(): modelSql[] {
        throw new Error("Method not implemented.");
    }
    createOne(entity: modelSql): modelSql {
        throw new Error("Method not implemented.");
    }
    getOneById(entity: modelSql): modelSql {
        throw new Error("Method not implemented.");
    }
    replaceOne(entity: modelSql): modelSql {
        throw new Error("Method not implemented.");
    }
    deleteOne(entity: modelSql): modelSql {
        throw new Error("Method not implemented.");
    }

}

class InMemoryEmployees<modelSql> implements IReposity<modelSql>{
    getAll(): modelSql[] {
        throw new Error("Method not implemented.");
    }
    createOne(entity: modelSql): modelSql {
        throw new Error("Method not implemented.");
    }
    getOneById(entity: modelSql): modelSql {
        throw new Error("Method not implemented.");
    }
    replaceOne(entity: modelSql): modelSql {
        throw new Error("Method not implemented.");
    }
    deleteOne(entity: modelSql): modelSql {
        throw new Error("Method not implemented.");
    }

}

let entity: employeeSql = { id: 1 };

let repository: IReposity<employeeSql> = new MysqlRepository<employeeSql>();
let userSql: employeeSql = repository.getOneById(entity);


//Adapter:
//Verifica que no existan valores nulos en las propiedades del objeto:
let employeeAdapter = new Employee();

if (userSql.id &&
    userSql.name &&
    userSql.email) {

    employeeAdapter.setId(userSql.id);
    employeeAdapter.setName(userSql.name);
    employeeAdapter.setEmail(userSql.email);
}

console.log(employeeAdapter);

